package com.marvel.developer.marvelheroes.adapter;

import android.content.Context;
import android.view.ViewGroup;

import com.marvel.developer.marvelheroes.model.StorySummary;
import com.marvel.developer.marvelheroes.view.StoryItemView;
import com.marvel.developer.marvelheroes.view.StoryItemView_;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.List;

@EBean
public class StoryAdapter extends RecyclerViewAdapterBase<StorySummary, StoryItemView> {

    @RootContext
    protected Context context;

    @Override
    public void onBindViewHolder(ViewWrapper<StoryItemView> holder, int position) {
        StoryItemView view = holder.getView();
        StorySummary storySummary = items.get(position);
        view.bind(storySummary);
    }


    @Override
    protected StoryItemView onCreateItemView(ViewGroup parent, int viewType) {
        return StoryItemView_.build(context);
    }


    public void setItems(List<StorySummary> storySummaries) {
        this.items = storySummaries;
    }
}