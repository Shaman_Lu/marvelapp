package com.marvel.developer.marvelheroes.activity;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.marvel.developer.marvelheroes.R;
import com.marvel.developer.marvelheroes.adapter.StoryAdapter;
import com.marvel.developer.marvelheroes.model.Character;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_hero_detail)
public class HeroDetailActivity extends AppCompatActivity {

    @Extra
    protected Character character;

    @ViewById
    protected ImageView ivThumb;

    @ViewById
    protected TextView tvName;

    @ViewById(R.id.scroll_view)
    protected ScrollView scrollView;

    @ViewById
    protected RecyclerView recyclerView;

    @Bean
    protected StoryAdapter adapter;

    @AfterViews
    void afterViews() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        if(character.getDescription() != null && !character.getDescription().isEmpty()){
            tvName.setText(character.getDescription());
        } else {
            tvName.setVisibility(View.GONE);
        }
        Picasso.with(this) //
                .load(character.getThumNail().getPath().concat(".").concat(character.getThumNail().getExtension())) //
                .error(R.drawable.logo) //
                .fit() //
                .tag(this) //
                .into(ivThumb);

        adapter.setItems(character.getStories().getItems());
        recyclerView.setAdapter(adapter);


        recyclerView.postDelayed(new Runnable() {
            @Override
            public void run() {
                scrollView.scrollTo(0,0);
            }
        },1);



    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
