package com.marvel.developer.marvelheroes.enumeration;

/**
 * Created by LuizAntonio on 25/05/2017.
 */

public enum OrderEnum {

    NAME("name"),
    MODIFIED("modified"),
    NAME_DESC("-name"),
    MODIFIED_DESC("-modified");

    private String value;

    OrderEnum (String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
