package com.marvel.developer.marvelheroes.adapter;

import android.content.Context;
import android.view.ViewGroup;

import com.marvel.developer.marvelheroes.activity.HeroListActivity;
import com.marvel.developer.marvelheroes.model.Character;
import com.marvel.developer.marvelheroes.view.CharacterItemView;
import com.marvel.developer.marvelheroes.view.CharacterItemView_;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.List;

@EBean
public class CharacterAdapter extends RecyclerViewAdapterBase<Character, CharacterItemView> {

    @RootContext
    protected Context context;

    private CharacterItemView.ItemClickListener listener;

    @Override
    public void onBindViewHolder(ViewWrapper<CharacterItemView> holder, int position) {
        CharacterItemView view = holder.getView();
        Character character = items.get(position);
        view.bind(character, listener);
    }


    @Override
    protected CharacterItemView onCreateItemView(ViewGroup parent, int viewType) {
        listener = (HeroListActivity)context;
        return CharacterItemView_.build(context);
    }


    public void setItems(List<Character> searchResults) {
        this.items = searchResults;
    }
}