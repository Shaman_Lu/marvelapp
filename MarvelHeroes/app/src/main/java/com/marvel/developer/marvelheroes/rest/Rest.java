package com.marvel.developer.marvelheroes.rest;

import android.content.Context;
import android.net.Uri;

import com.google.gson.GsonBuilder;
import com.marvel.developer.marvelheroes.R;
import com.marvel.developer.marvelheroes.rest.interceptor.AuthenticatorOAuthInterceptor;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.res.StringRes;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@EBean(scope = EBean.Scope.Singleton)
public class Rest {

    @RootContext
    protected Context context;

    @Bean
    protected AuthenticatorOAuthInterceptor authenticatorOauthInterceptor;

    @StringRes(R.string.api_url)
    protected String apiUrl;

    private OAuthApi oAuthApi;

    @AfterInject
    void afterInjection() {
        buildApi();
    }

    private OkHttpClient getClient(HttpLoggingInterceptor logJson, Interceptor authentication) {

        return new OkHttpClient.Builder()
                .addNetworkInterceptor(logJson)
                .addNetworkInterceptor(authentication)
                .build();
    }

    private Retrofit getRetrofit(OkHttpClient client, String url) {
        return new Retrofit.Builder()
                .client(client)
                .baseUrl(url)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                //use for convert JSON file into object
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder()
                        .create()))
                .build();
    }

    public void buildApi() {
        HttpLoggingInterceptor loggingJson = new HttpLoggingInterceptor();
        // set your desired log level
        loggingJson.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = getClient(loggingJson, authenticatorOauthInterceptor);

        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https").authority(apiUrl);
        String url = builder.build().toString();

        oAuthApi = getRetrofit(client, url).create(OAuthApi.class);
    }

    public OAuthApi getoAuthApi() {
        return oAuthApi;
    }
}