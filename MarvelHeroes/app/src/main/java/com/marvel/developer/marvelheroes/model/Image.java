
package com.marvel.developer.marvelheroes.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Image implements Serializable {

    @SerializedName("path")
    private String path;
    @SerializedName("extension")
    private String extension;
    private final static long serialVersionUID = -607890672772660106L;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

}
