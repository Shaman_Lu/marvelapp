package com.marvel.developer.marvelheroes.rest;

import com.marvel.developer.marvelheroes.model.CharacterDataWrapper;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface OAuthApi {

    @GET("v1/public/characters")
    Call<CharacterDataWrapper> getCharacters(@Query("name") String name,
                                             @Query("nameStartsWith") String nameStartsWith,
                                             @Query("modifiedSince") String modifiedSince,
                                             @Query("comics") String comics,
                                             @Query("series") String series,
                                             @Query("events") String events,
                                             @Query("stories") String stories,
                                             @Query("orderBy") String orderBy,
                                             @Query("limit") String limit,
                                             @Query("offset") String offset);

}
