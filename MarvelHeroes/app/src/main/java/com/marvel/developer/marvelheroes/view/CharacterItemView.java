package com.marvel.developer.marvelheroes.view;

import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.marvel.developer.marvelheroes.R;
import com.marvel.developer.marvelheroes.model.Character;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;


@EViewGroup(R.layout.item_view_character_search)
public class CharacterItemView extends LinearLayout {

    @ViewById(R.id.tv_name)
    protected TextView tvName;

    @ViewById(R.id.iv_thumb)
    protected ImageView ivThumb;

    private ItemClickListener itemClickListener;

    private Character character;

    private Context context;

    public CharacterItemView(Context context) {
        super(context);
        this.context = context;
    }

    public void bind(final Character character, ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
        this.character = character;
        tvName.setText(character.getName());
        ivThumb.postDelayed(new Runnable() {
            @Override
            public void run() {
                Picasso.with(context) //
                        .load(character.getThumNail().getPath().concat(".").concat(character.getThumNail().getExtension())) //
                        .error(R.drawable.logo) //
                        .fit() //
                        .tag(context) //
                        .into(ivThumb);
            }
        },1);
    }

    @Click(R.id.card_view)
    void onClickItem() {
        itemClickListener.onItemClicked(character);
    }

    public interface ItemClickListener {
        void onItemClicked(Character searchResult);
    }
}