
package com.marvel.developer.marvelheroes.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SeriesList implements Serializable
{

    @SerializedName("available")
    private String availaLe;
    @SerializedName("returned")
    private String returned;
    @SerializedName("collectionURI")
    private String collectionURI;
    @SerializedName("items")
    private List<StorySummary> items = new ArrayList<StorySummary>();
    private final static long serialVersionUID = -581931427555919651L;

    public String getAvailaLe() {
        return availaLe;
    }

    public void setAvailaLe(String availaLe) {
        this.availaLe = availaLe;
    }

    public String getReturned() {
        return returned;
    }

    public void setReturned(String returned) {
        this.returned = returned;
    }

    public String getCollectionURI() {
        return collectionURI;
    }

    public void setCollectionURI(String collectionURI) {
        this.collectionURI = collectionURI;
    }

    public List<StorySummary> getItems() {
        return items;
    }

    public void setItems(List<StorySummary> items) {
        this.items = items;
    }
}
