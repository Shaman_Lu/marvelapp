
package com.marvel.developer.marvelheroes.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class StorySummary implements Serializable {

    @SerializedName("resourceURI")
    private String resourceURI;
    @SerializedName("name")
    private String name;
    @SerializedName("type")
    private String type;
    private final static long serialVersionUID = -8790146281836114941L;

    public String getResourceURI() {
        return resourceURI;
    }

    public void setResourceURI(String resourceURI) {
        this.resourceURI = resourceURI;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


}
