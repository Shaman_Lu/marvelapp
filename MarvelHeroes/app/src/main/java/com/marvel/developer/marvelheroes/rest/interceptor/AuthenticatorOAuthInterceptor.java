package com.marvel.developer.marvelheroes.rest.interceptor;

import android.content.Context;

import com.marvel.developer.marvelheroes.R;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

@EBean
public class AuthenticatorOAuthInterceptor implements Interceptor {

    @RootContext
    protected Context context;


    @Override
    public Response intercept(Chain chain) throws IOException {
        Response response = null;
        try {
            Integer ts = new Random().nextInt();
            String string = ts + context.getString(R.string.private_key).concat(context.getString(R.string.public_key));

            MessageDigest m = MessageDigest.getInstance("MD5");

            m.update(string.getBytes(), 0, string.length());


            Request original = chain.request();
            HttpUrl originalHttpUrl = original.url();
            HttpUrl url = originalHttpUrl.newBuilder()
                    .addQueryParameter("ts", String.valueOf(ts))
                    .addQueryParameter("apikey", context.getString(R.string.public_key))
                    .addQueryParameter("hash", new BigInteger(1, m.digest()).toString(16))
                    .build();

            Request.Builder requestBuilder = original.newBuilder().url(url);
            requestBuilder.addHeader("Accept", "application/json");

            Request request = requestBuilder.build();
            response = chain.proceed(request);

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return response;
    }

}