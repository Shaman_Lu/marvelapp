package com.marvel.developer.marvelheroes.view;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.marvel.developer.marvelheroes.R;
import com.marvel.developer.marvelheroes.model.StorySummary;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;


@EViewGroup(R.layout.item_view_story)
public class StoryItemView extends LinearLayout {

    @ViewById(R.id.tv_name)
    protected TextView tvName;


    public StoryItemView(Context context) {
        super(context);
    }

    public void bind(final StorySummary storySummary) {
        tvName.setText(storySummary.getName());
    }


}