package com.marvel.developer.marvelheroes.activity;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.marvel.developer.marvelheroes.R;
import com.marvel.developer.marvelheroes.adapter.CharacterAdapter;
import com.marvel.developer.marvelheroes.enumeration.OrderEnum;
import com.marvel.developer.marvelheroes.model.Character;
import com.marvel.developer.marvelheroes.model.CharacterDataWrapper;
import com.marvel.developer.marvelheroes.rest.Rest;
import com.marvel.developer.marvelheroes.view.CharacterItemView;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@EActivity(R.layout.activity_hero_list)
public class HeroListActivity extends AppCompatActivity implements CharacterItemView.ItemClickListener {

    @ViewById
    protected Toolbar toolbar;

    @ViewById(R.id.search_view)
    protected MaterialSearchView searchView;

    @ViewById(R.id.progressBar)
    protected ProgressBar progressBar;

    @ViewById(R.id.recycler_view)
    protected RecyclerView recyclerView;

    @Bean
    protected CharacterAdapter adapter;

    @Bean
    protected Rest rest;

    @AfterViews
    void afterViews() {
        setSupportActionBar(toolbar);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        setupSearch();
        return super.onCreateOptionsMenu(menu);
    }

    private void setupSearch() {
        ImageView ivArrowBack = (ImageButton) searchView.findViewById(R.id.action_up_btn);
        ivArrowBack.setImageResource(R.drawable.ic_search);
        ivArrowBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        searchView.setCursorDrawable(R.drawable.cursor_drawable);
        searchView.showSearch(true);

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                if (query.length() > 2) {
                    showProgress();
                    getCharacters(query);
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                if (query.length() > 2) {
                    showProgress();
                    getCharacters(query);
                }
                return true;
            }
        });
    }

    @UiThread
    public void updateAdapter(List<Character> characters) {
        adapter.setItems(characters);
        recyclerView.setAdapter(adapter);
    }

    @UiThread
    void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @UiThread
    void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Background
    void getCharacters(String name) {
        Call<CharacterDataWrapper> call = rest.getoAuthApi().getCharacters(null, name, null, null, null, null, null, OrderEnum.NAME.toString(), "20", null);
        call.enqueue(new Callback<CharacterDataWrapper>() {

            @Override
            public void onResponse(Call<CharacterDataWrapper> call, Response<CharacterDataWrapper> response) {
                if (response.isSuccessful()) {
                    updateAdapter(response.body().getData().getResults());
                } else {
                    try {
                        Toast.makeText(HeroListActivity.this, response.errorBody().string(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<CharacterDataWrapper> call, Throwable t) {
                hideProgress();
                Toast.makeText(HeroListActivity.this, "Falha", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onItemClicked(Character character) {
        HeroDetailActivity_.intent(this).character(character).start();
    }
}
