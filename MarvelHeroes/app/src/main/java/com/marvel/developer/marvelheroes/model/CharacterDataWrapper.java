
package com.marvel.developer.marvelheroes.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CharacterDataWrapper implements Serializable {

    @SerializedName("code")
    private String code;
    @SerializedName("status")
    private String status;
    @SerializedName("copyright")
    private String copyright;
    @SerializedName("attributionText")
    private String attriUtionText;
    @SerializedName("attributionHTML")
    private String attriUtionHTML;
    @SerializedName("data")
    private CharacterDataContainer data;
    @SerializedName("etag")
    private String etag;
    private final static long serialVersionUID = 4984469529942555972L;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCopyright() {
        return copyright;
    }

    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }

    public String getAttriUtionText() {
        return attriUtionText;
    }

    public void setAttriUtionText(String attriUtionText) {
        this.attriUtionText = attriUtionText;
    }

    public String getAttriUtionHTML() {
        return attriUtionHTML;
    }

    public void setAttriUtionHTML(String attriUtionHTML) {
        this.attriUtionHTML = attriUtionHTML;
    }

    public CharacterDataContainer getData() {
        return data;
    }

    public void setData(CharacterDataContainer data) {
        this.data = data;
    }

    public String getEtag() {
        return etag;
    }

    public void setEtag(String etag) {
        this.etag = etag;
    }

    @Override
    public String toString() {
        return "CharacterDataWrapper{" +
                "code='" + code + '\'' +
                ", status='" + status + '\'' +
                ", copyright='" + copyright + '\'' +
                ", attriUtionText='" + attriUtionText + '\'' +
                ", attriUtionHTML='" + attriUtionHTML + '\'' +
                ", data=" + data +
                ", etag='" + etag + '\'' +
                '}';
    }
}
