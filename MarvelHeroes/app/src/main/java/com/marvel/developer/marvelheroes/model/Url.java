
package com.marvel.developer.marvelheroes.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class Url implements Serializable {

    @SerializedName("type")
    private String type;
    @SerializedName("url")
    private String url;
    private final static long serialVersionUID = 2242562832738856252L;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


}
