
package com.marvel.developer.marvelheroes.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CharacterDataContainer implements Serializable {

    @SerializedName("offset")
    private String offset;
    @SerializedName("limit")
    private String limit;
    @SerializedName("total")
    private String total;
    @SerializedName("count")
    private String count;
    @SerializedName("results")
    private List<Character> results = new ArrayList<Character>();
    private final static long serialVersionUID = 478092665561975535L;

    public String getOffset() {
        return offset;
    }

    public void setOffset(String offset) {
        this.offset = offset;
    }

    public String getLimit() {
        return limit;
    }

    public void setLimit(String limit) {
        this.limit = limit;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public List<Character> getResults() {
        return results;
    }

    public void setResults(List<Character> results) {
        this.results = results;
    }
}
